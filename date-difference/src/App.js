import React, { Component } from 'react';
import './App.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class App extends Component {
    constructor(props){
        super(props);
        this.state={startDate:null, endDate:null,difference:null,showMessage:false};
        this.handleStartDateChange=this.handleStartDateChange.bind(this);
        this.handleEndDateChange=this.handleEndDateChange.bind(this);
        this.calculate=this.calculate.bind(this);
    }  

    handleStartDateChange(e){
        this.setState({startDate:e,difference:null,showMessage:false});
    }

    handleEndDateChange(e){
        this.setState({endDate:e,difference:null,showMessage:false});
    }

    //function to calculate difference in dates
    calculate(e){
        var startDate = this.state.startDate;
        var endDate = this.state.endDate;
        var daysDifference = null;
        if(startDate!==null){
            startDate=new Date(this.state.startDate);
        }
        if(endDate!==null){
            endDate=new Date(this.state.endDate);
        }
        if(startDate === null || endDate === null){
            this.setState({showMessage:true});
			return;
        }
        else if(startDate !== null || endDate !== null){
            daysDifference= Math.floor((Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()) - Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) ) /(1000 * 60 * 60 * 24));               
            this.setState({difference:daysDifference,showMessage:false});
        }        
    }

  render() {
        const diff=this.state.difference;
		return (
        <div className="App">
          <DatePicker
                selected={this.state.startDate}
                name="startDate"
                dateFormat="dd MM yyyy"
                onChange={this.handleStartDateChange}
                placeholderText="Select a date"
                minDate={new Date("01/01/1990")}
                maxDate={new Date("12/31/2010")}
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                onKeyDown={e => e.preventDefault()}              
            />

           <DatePicker
                selected={this.state.endDate}
                name="endDate"
                dateFormat="dd MM yyyy"
                onChange={this.handleEndDateChange}
                placeholderText="Select a date"
                minDate={new Date("01/01/1990")}
                maxDate={new Date("12/31/2010")}
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                onKeyDown={e => e.preventDefault()}
            />
            
            <button onClick={this.calculate}>Calculate Difference In Dates </button>
            {(diff!==null) && <p>Difference in days {diff}</p>}
            {(diff===null) && (this.state.showMessage===true) && <p>Enter date in both fields</p>}
        </div>
    );
  }
}

export default App;