import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import TestRenderer from 'react-test-renderer';
import DatePicker from "react-datepicker";
import Enzyme,{shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('render DatePicker correctly', () => { 
    const TextInputComponent = TestRenderer.create(<DatePicker />).toJSON();
    expect(TextInputComponent).toMatchSnapshot();
});

it('render paragraph correctly', () => { 
    const paragraphComponent = TestRenderer.create(<p></p>).toJSON();
    expect(paragraphComponent).toMatchSnapshot();
});

describe('<App />',()=>{
it('renders 1 <App /> component',()=>{
const component = shallow(<App />);
expect(component).toHaveLength(1);
})
});

describe('difference',()=>{
it('null in start date should give null in difference', () => {
    const wrapper = mount(<App />);
    const startDate= new Date("05 11 2008");
    const endDate= new Date("05 12 2008");
    wrapper.setState({startDate:startDate, endDate:endDate,difference:null})
    wrapper.update();
    wrapper.find('button').simulate('click');
    expect(wrapper.state().difference).toEqual(1);
  });
});

describe('difference',()=>{
it('null in end date should give null in difference', () => {
    const wrapper = mount(<App />);
    const startDate= new Date("05 11 2008");
    wrapper.setState({startDate:startDate, endDate:null,difference:null})
    wrapper.update();
    wrapper.find('button').simulate('click');
    expect(wrapper.state().difference).toEqual(null);
  });
});

describe('difference',()=>{
it('null in both start and end date should give null in difference', () => {
    const wrapper = mount(<App />);
    wrapper.setState({startDate:null, endDate:null,difference:null})
    wrapper.update();
    wrapper.find('button').simulate('click');
    expect(wrapper.state().difference).toEqual(null);
  });
});

describe('setting dates',()=>{
    it('set start date', () => {
        const wrapper = mount(<App />);
        const startDate= new Date("05 11 2008");
        wrapper.setState({startDate:null, endDate:null,difference:null})
        wrapper.update();   
        wrapper.instance().handleStartDateChange(startDate);
        expect(wrapper.state().startDate).toBe(startDate);
    })

    it('set end date', () => {
        const wrapper = mount(<App />);
        const endDate= new Date("06 11 2008");
        wrapper.setState({startDate:null, endDate:null,difference:null})
        wrapper.update();   
        wrapper.instance().handleEndDateChange(endDate);
        expect(wrapper.state().endDate).toBe(endDate);
    })
    
});